package org.amdatu.chat.wordcounter.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.amdatu.chat.wordcounter.WordCounter;
import org.apache.felix.dm.annotation.api.Component;

import com.google.common.collect.Sets;

@Component
public class LamdaWordCounter implements WordCounter {
	private final static Set<String> IGNORES = Sets.newHashSet("is", "this", "and", "or", "a");
	
	@Override
	public String getMostPopular(List<String> lines) {
		Map<String, Long> collect = lines
				.stream()
				.flatMap(l -> Stream.of(l.split(" ")))
				.filter(s -> !IGNORES.contains(s))
				.collect(
						Collectors.groupingBy(String::toString,
								Collectors.counting()));

		Optional<String> popular = collect.entrySet().stream()
				.max((a, b) -> Long.compare(a.getValue(), b.getValue()))
				.map(Entry::getKey);

		return popular.orElse("");
	}
}
